const express = require('express'); 
const router = express.Router();
const mongoose = require('mongoose'); 
const Recipe = require('../models/recipe'); 
const auth = require('../middleware/auth');


// Post one
router.post('/create', auth, async(req, res) => {{

    try {
        const {title, subtitle, description, cookingTime} = req.body;
        if(!title )
            return res.status(400).json({message : "Veillez à remplir les champs obligatoires !"})
        
        const newRecipe = new Recipe({
            title,
            subtitle, 
            description, 
            cookingTime,
            userId : req.user
        });

        const savedRecipe = await newRecipe.save();
        res.status(200).json(savedRecipe);         
        
    } catch (error) {
        res.status(500).json({error: error.message});        
    }

}});

// Get all
router.get('/' , async(req, res) => {
    try {
        items = await Recipe.find({});
        res.status(200).json(items);
    } catch (error) {
        res.status(400).json({error: error.message});
    }   
});


// Get all recipes from a specific user id
router.get('/my-recipies', auth, async(req,res) => {
    try {
        const recipies = await Recipe.find({userId : req.user});
        res.json(recipies);
        
    } catch (error) {
        res.status(500).json({message : "Pas de recette pour cet utilisateur"});        
    }
});

// Get one recipes from a specific user id


// Delete 
router.delete('/delete/:id', auth, async(req, res) => {
    const item = await Recipe.findOne({userId: req.user, _id: req.params.id});
    if(!item)
        return res.status(400).json({message : "Aucune recette trouvée pour cet utilisateur"})
    const deletedRecipe = await item.delete(req.params.id);
    res.json(deletedRecipe);
});


// update

module.exports = router;